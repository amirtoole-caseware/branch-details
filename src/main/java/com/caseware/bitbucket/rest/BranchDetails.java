package com.caseware.bitbucket.rest;

import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.scm.git.command.GitCommand;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;


@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class BranchDetails {

    private final RepositoryService _repoService;
    private final GitCommandBuilderFactory _gitCommandBuilderFactory;
    private final NavBuilder _navBuilder;

    public interface BranchResponse {
        @JsonProperty("baseUrl")
        String baseUrl();

        @JsonProperty("branches")
        List<String> branches();
    }

    public BranchDetails(final GitCommandBuilderFactory gitCommandBuilderFactory, final RepositoryService repoService, final NavBuilder navBuilder) {
        _gitCommandBuilderFactory = gitCommandBuilderFactory;
        _repoService = repoService;
        _navBuilder = navBuilder;
    }

    @GET
    @Path("projects/{projectKey}/repos/{repoSlug}/commits/{guid}/value")
    public BranchResponse getBranchForCommit(@PathParam("projectKey") final String projectKey, @PathParam("repoSlug") final String repoSlug, @PathParam("guid") final String guid) {
        final Repository repo = _repoService.getBySlug(projectKey, repoSlug);

        final NavBuilder.ListCommits listCommits = _navBuilder.repo(repo).commits();
        final GitCommand<String> command = _gitCommandBuilderFactory.builder(repo)
                .command("branch")
                .argument("--contains")
                .argument(guid)
                .build(new GitStringOutputHandler());

        final String strResult = command.call();
        if (strResult == null || strResult.isEmpty()) {
            return null;
        }
        final String[] arrBranches = strResult.split("\n");
        final List<String> lstBranches = new ArrayList<>();
        for (int i = 0; i < arrBranches.length; i++) {
            lstBranches.add(arrBranches[i].trim().replace("*",""));
        }
        return new BranchResponse() {

            @Override
            public String baseUrl() {
                return listCommits.buildAbsolute();
            }

            @Override
            public List<String> branches() {
                return lstBranches;
            }
        };
    }
}
