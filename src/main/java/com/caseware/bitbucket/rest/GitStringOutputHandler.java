package com.caseware.bitbucket.rest;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.StringOutputHandler;
import com.atlassian.utils.process.Watchdog;

import java.io.InputStream;

/**
 * Simple output handler that just returns the output value from the git process, trimmed to null if whitespace only.
 * 
 * source: https://bitbucket.org/tpettersen/stashlas-camp/raw/5206df7956328d7db8dc4f8d6bba890c5a474574/src/main/java/com/atlassian/stash/atlascamp/notes/ShowNoteOutputHandler.java
 */
public class GitStringOutputHandler implements CommandOutputHandler<String> {

	private final StringOutputHandler outputHandler;

	public GitStringOutputHandler() {
		this.outputHandler = new StringOutputHandler();
	}

	@Override
	public String getOutput() {
		String output = outputHandler.getOutput();

		// trim to null
		if (output != null && output.trim().isEmpty()) {
			output = null;
		}
		return output;
	}

	@Override
	public void process(final InputStream output) throws ProcessException {
		outputHandler.process(output);
	}

	@Override
	public void complete() throws ProcessException {
		outputHandler.complete();
	}

	@Override
	public void setWatchdog(final Watchdog watchdog) {
		outputHandler.setWatchdog(watchdog);
	}
}