define('plugin/branchDetails', [
    'jquery',
    'aui',
    'bitbucket/util/state',
    'bitbucket/util/navbuilder',
    'exports'
], function(
    $,
    AJS,
    pageState,
    navBuilder,
    exports
) {
    exports.onReady = function () {
        var url = AJS.contextPath() + "/rest/branchForCommit/1.0"
            + "/projects/" + encodeURIComponent(pageState.getProject().key)
            + "/repos/" + encodeURIComponent(pageState.getRepository().slug)
            + "/commits/" + pageState.getRef().id + "/value";
        $.ajax({
            type: 'GET',
            url: url,
            success: function(data) {
                   if (data == null)
                    return;

                   var result = '<span class="aui-icon aui-icon-small aui-iconfont-devtools-branch" title="Branches"></span>';
                   for( var i = 0; i < data.branches.length; i++  ) {
                       result += '<a href="' +  data.baseUrl + '?until=refs/heads/' + data.branches[i] + '" '
                                             + 'class="ch-info-branch branch-info-branch-single">'
                                             + data.branches[i]
                                             + '</a>';
                   }
                   $('.branch-info-details').html(result);
            },
            error: function() {
                console.error("Error GETing branches from branchDetails plugin.");
            },
            contentType: "application/json"
        });
    }
});

AJS.$(function() {
    require('plugin/branchDetails').onReady();
});
